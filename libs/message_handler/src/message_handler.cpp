#include "message_handler/message_handler.hpp"

#include <optional>
#include <thread>
#include <cmath>

#include "nlohmann/json.hpp"
#include "gcs/frames/rotor_ctl_schema.h"
#include "gcs/socket/socket_base.hpp"
#include "spdlog/spdlog.h"
#include "fmt/format.h"
#include "serial/serial_base.hpp"

namespace message_handler {

using namespace std::chrono_literals;

constexpr auto elevation_max = 180.0f;
constexpr auto elevation_min = -15.0f;

message_handler::message_handler(std::unique_ptr<gcs::socket::socket_base> pub_socket,
                                 std::unique_ptr<serial::serial_base> serial) :
    _pub_socket(std::move(pub_socket)),
    _serial(std::move(serial)) {
    _handlers["rotor_ctl"] = [this](const std::string& data) {
        this->handle_control_rotator(data);
    };
}

void message_handler::handle(const gcs::socket::message& msg) {
    if (auto handler = _handlers.find(msg.topic); handler != _handlers.end()) {
        handler->second(msg.data);
    }
}

float clamp_elevation_angle(float angle) {
    return std::clamp(angle, elevation_min, elevation_max);
}

float reduce_azimuth_angle(float angle) {
    angle = std::fmod(angle, 360.0f);
    if (angle < 0.0f) {
        angle += 360.0f;
    }
    return angle;
}

void message_handler::handle_control_rotator(const std::string& msg) {
    try {
        gcs::frames::rotor_ctl data = nlohmann::json::parse(msg);
        if (data.has_azimuth()) {
            auto azimuth = reduce_azimuth_angle(data.get_azimuth().value());
            spdlog::info("Received 'rotor_ctl', azimuth: {}", azimuth);
            auto command = fmt::format("AZ{:.1f}\n", azimuth);
            spdlog::info("Command:\n{}", command);
            _serial->write(std::as_bytes(std::span(command.data(), command.size())));
            std::this_thread::sleep_for(10ms);
        }

        if (data.has_elevation()) {
            auto elevation = clamp_elevation_angle(data.get_elevation().value());
            spdlog::info("Received 'rotor_ctl', elevation: {}", elevation);
            auto command = fmt::format("EL{:.1f}\n", elevation);
            spdlog::info("Command:\n{}", command);
            _serial->write(std::as_bytes(std::span(command.data(), command.size())));
        }
    } catch (const std::exception& e) {
        spdlog::error("Rotator control error: {}", e.what());
    }
}

std::unique_ptr<gcs::socket::socket_base> message_handler::get_pub_socket() {
    return std::move(_pub_socket);
}

} // namespace message_handler
