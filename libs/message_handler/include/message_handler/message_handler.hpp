#pragma once
#include <functional>
#include <map>
#include <memory>
#include <string_view>

#include "message_handler/message_handler_base.hpp"

namespace gcs::socket {
class socket_base;
}

namespace serial {
class serial_base;
}

namespace gpio {
class gpio_interface;
}

namespace message_handler {
class message_handler final : public message_handler_base {
public:
    message_handler(std::unique_ptr<gcs::socket::socket_base> pub_socket,
                    std::unique_ptr<serial::serial_base> serial);
    message_handler(const message_handler&) = delete;
    message_handler& operator=(const message_handler&) = delete;
    message_handler(message_handler&&) noexcept = delete;
    message_handler& operator=(message_handler&&) noexcept = delete;
    ~message_handler() = default;

    void handle(const gcs::socket::message& msg) override;
    std::unique_ptr<gcs::socket::socket_base> get_pub_socket();

private:
    void handle_control_rotator(const std::string& msg);

    std::unique_ptr<gcs::socket::socket_base> _pub_socket;
    std::unique_ptr<serial::serial_base> _serial;
    std::map<std::string_view, std::function<void(const std::string&)>> _handlers;
};
} // namespace message_handler
