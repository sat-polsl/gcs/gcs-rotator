#pragma once

namespace gcs::socket {
struct message;
}

namespace message_handler {
class message_handler_base {
public:
    virtual void handle(const gcs::socket::message& message) = 0;

protected:
    ~message_handler_base() = default;
};
} // namespace message_handler
