#include "serial/serial.hpp"
#include <span>

namespace serial {

serial::serial(std::string port) : _ctx{}, _serial{_ctx} { _serial.open(port); }

void serial::set_baudrate(std::uint32_t baudrate) {
    _serial.set_option(boost::asio::serial_port_base::baud_rate(baudrate));
}

void serial::close() { _serial.close(); }
void serial::write(std::span<const std::byte> buffer) {
    _serial.write_some(boost::asio::buffer(buffer.data(), buffer.size()));
}

std::span<const std::byte> serial::read(std::span<std::byte> buffer) {
    auto size = _serial.read_some(boost::asio::buffer(buffer.data(), buffer.size()));
    return buffer.subspan(0, size);
}

} // namespace serial
