#pragma once

#include <boost/asio/serial_port.hpp>
#include "serial/serial_base.hpp"

namespace serial {
class serial : public serial_base {
public:
    serial(std::string port);

    void write(std::span<const std::byte> buffer);
    std::span<const std::byte> read(std::span<std::byte> buffer);

    void set_baudrate(std::uint32_t baudrate);
    void close();

private:
    boost::asio::io_context _ctx;
    boost::asio::serial_port _serial;
};
} // namespace serial