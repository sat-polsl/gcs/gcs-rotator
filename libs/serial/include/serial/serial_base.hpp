#pragma once

#include <span>
#include <cstdint>

namespace serial {
class serial_base {
public:
    virtual void write(std::span<const std::byte> buffer) = 0;
    virtual std::span<const std::byte> read(std::span<std::byte> buffer) = 0;
    virtual void set_baudrate(std::uint32_t baudrate) = 0;
    virtual void close() = 0;
    virtual ~serial_base() {}
};
} // namespace serial
