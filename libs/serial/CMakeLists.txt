set(TARGET serial)

add_library(${TARGET} STATIC)

target_sources(${TARGET}
    PRIVATE
    src/serial.cpp
    )

target_include_directories(${TARGET}
    PUBLIC
    include
    )
