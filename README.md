# GCS Rotator

## Building
This application is only **Linux compatible** (on Windows use [WSL](https://learn.microsoft.com/en-us/windows/wsl/install) to build)

### Dependencies
- gcc 11 or higher
- pkgconfig
- libgnutls30
- libsodium-dev
- boost-dev

## Configuration
Environment variables configuration:

* **PUB_PORT** - Connect **PUBLISHER** type sockets to this port.
* **SUB_PORT** - Connect **SUBSCRIBER** type sockets to this port.
* **PROXY_ENDPOINT** - ZMQ proxy address.

## Build and run with docker

1. Build: `docker-compose build`
2. Run: `docker-compose up -d`



