#include "gtest/gtest.h"
#include "mock/serial.hpp"
#include "mock/socket.hpp"
#include "message_handler/message_handler.hpp"
#include <memory>
#include <array>

namespace {
using namespace testing;

using socket_mock = StrictMock<gcs::socket::mock::socket>;
using serial_mock = StrictMock<serial::mock::serial>;

struct MessageHandlerTest : public Test {
    socket_mock* _socket{new socket_mock()};
    serial_mock* _serial{new serial_mock()};

    std::unique_ptr<socket_mock> _socket_ptr{_socket};
    std::unique_ptr<serial_mock> _serial_ptr{_serial};
};

TEST_F(MessageHandlerTest, EmptyJson) {
    message_handler::message_handler handler(std::move(_socket_ptr), std::move(_serial_ptr));
    gcs::socket::message msg{.topic = "rotor_ctl", .data = "{}"};
    handler.handle(msg);
}

TEST_F(MessageHandlerTest, WrongTopic) {
    message_handler::message_handler handler(std::move(_socket_ptr), std::move(_serial_ptr));
    gcs::socket::message msg{.topic = "abcd", .data = "{}"};
    handler.handle(msg);
}

TEST_F(MessageHandlerTest, RotatorControlMessageWithZeroes) {
    message_handler::message_handler handler(std::move(_socket_ptr), std::move(_serial_ptr));
    gcs::socket::message msg{.topic = "rotor_ctl", .data = "{\"azimuth\":0.0,\"elevation\":0.0}"};
    InSequence s;
    EXPECT_CALL(*_serial, write(_)).WillOnce(Invoke([](std::span<const std::byte> buffer) {
        EXPECT_THAT(buffer.size(), Eq(6));
        auto str = std::string_view(reinterpret_cast<const char*>(buffer.data()), buffer.size());
        EXPECT_THAT(str, Eq(std::string_view("AZ0.0\n")));
    }));
    EXPECT_CALL(*_serial, write(_)).WillOnce(Invoke([](std::span<const std::byte> buffer) {
        EXPECT_THAT(buffer.size(), Eq(6));
        auto str = std::string_view(reinterpret_cast<const char*>(buffer.data()), buffer.size());
        EXPECT_THAT(str, Eq(std::string_view("EL0.0\n")));
    }));
    handler.handle(msg);
}

TEST_F(MessageHandlerTest, RotatorControlMessageWithPositiveValues) {
    message_handler::message_handler handler(std::move(_socket_ptr), std::move(_serial_ptr));
    gcs::socket::message msg{.topic = "rotor_ctl",
                            .data = "{\"azimuth\":12.345,\"elevation\":67.891}"};
    InSequence s;
    EXPECT_CALL(*_serial, write(_)).WillOnce(Invoke([](std::span<const std::byte> buffer) {
        EXPECT_THAT(buffer.size(), Eq(7));
        auto str = std::string_view(reinterpret_cast<const char*>(buffer.data()), buffer.size());
        EXPECT_THAT(str, Eq(std::string_view("AZ12.3\n")));
    }));
    EXPECT_CALL(*_serial, write(_)).WillOnce(Invoke([](std::span<const std::byte> buffer) {
        EXPECT_THAT(buffer.size(), Eq(7));
        auto str = std::string_view(reinterpret_cast<const char*>(buffer.data()), buffer.size());
        EXPECT_THAT(str, Eq(std::string_view("EL67.9\n")));
    }));
    handler.handle(msg);
}

TEST_F(MessageHandlerTest, RotatorControlMessageWithNegativeValues) {
    message_handler::message_handler handler(std::move(_socket_ptr), std::move(_serial_ptr));
    gcs::socket::message msg{.topic = "rotor_ctl",
                            .data = "{\"azimuth\":-12.345,\"elevation\":-67.891}"};
    InSequence s;
    EXPECT_CALL(*_serial, write(_)).WillOnce(Invoke([](std::span<const std::byte> buffer) {
        EXPECT_THAT(buffer.size(), Eq(8));
        auto str = std::string_view(reinterpret_cast<const char*>(buffer.data()), buffer.size());
        EXPECT_THAT(str, Eq(std::string_view("AZ347.7\n")));
    }));
    EXPECT_CALL(*_serial, write(_)).WillOnce(Invoke([](std::span<const std::byte> buffer) {
        EXPECT_THAT(buffer.size(), Eq(8));
        auto str = std::string_view(reinterpret_cast<const char*>(buffer.data()), buffer.size());
        EXPECT_THAT(str, Eq(std::string_view("EL-15.0\n")));
    }));
    handler.handle(msg);
}

TEST_F(MessageHandlerTest, RotatorControlEmpty) {
    message_handler::message_handler handler(std::move(_socket_ptr), std::move(_serial_ptr));
    gcs::socket::message msg{.topic = "rotor_ctl", .data = "{}"};
    handler.handle(msg);
}

TEST_F(MessageHandlerTest, RotatorControlMessageWithAzimuth) {
    message_handler::message_handler handler(std::move(_socket_ptr), std::move(_serial_ptr));
    gcs::socket::message msg{.topic = "rotor_ctl", .data = "{\"azimuth\":12.345}"};
    EXPECT_CALL(*_serial, write(_)).WillOnce(Invoke([](std::span<const std::byte> buffer) {
        EXPECT_THAT(buffer.size(), Eq(7));
        auto str = std::string_view(reinterpret_cast<const char*>(buffer.data()), buffer.size());
        EXPECT_THAT(str, Eq(std::string_view("AZ12.3\n")));
    }));
    handler.handle(msg);
}

TEST_F(MessageHandlerTest, RotatorControlMessageWithElevation) {
    message_handler::message_handler handler(std::move(_socket_ptr), std::move(_serial_ptr));
    gcs::socket::message msg{.topic = "rotor_ctl", .data = "{\"elevation\":67.891}"};
    InSequence s;
    EXPECT_CALL(*_serial, write(_)).WillOnce(Invoke([](std::span<const std::byte> buffer) {
        EXPECT_THAT(buffer.size(), Eq(7));
        auto str = std::string_view(reinterpret_cast<const char*>(buffer.data()), buffer.size());
        EXPECT_THAT(str, Eq(std::string_view("EL67.9\n")));
    }));
    handler.handle(msg);
}

} // namespace
