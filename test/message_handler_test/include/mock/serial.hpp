#pragma once
#include "serial/serial_base.hpp"
#include "gmock/gmock.h"

namespace serial::mock {
struct serial : public serial_base {
    serial() = default;
    ~serial() override = default;

    MOCK_METHOD(void, write, (std::span<const std::byte> buffer));
    MOCK_METHOD(std::span<const std::byte>, read, (std::span<std::byte> buffer));
    MOCK_METHOD(void, set_baudrate, (std::uint32_t baudrate));
    MOCK_METHOD(void, close, ());
};
} // namespace serial::mock
