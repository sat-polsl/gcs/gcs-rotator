#pragma once
#include "gcs/socket/socket_base.hpp"
#include "gmock/gmock.h"

namespace gcs::socket::mock {
struct socket : public socket_base {
    socket() = default;
    ~socket() override = default;

    MOCK_METHOD(std::optional<message>, receive, ());
    MOCK_METHOD(bool, send, (const message&));
    MOCK_METHOD(void, subscribe, (const std::string& topic));
    MOCK_METHOD(void, set_receive_timeout, (std::chrono::milliseconds));
    MOCK_METHOD(void, close, ());
};
} // namespace gcs_socket::mock
