#include "application.hpp"

#include <csignal>
#include <cstdio>
#include <thread>

#include "cxxopts.hpp"
#include "spdlog/spdlog.h"
#include "gcs/socket/context.hpp"

namespace application {

using namespace std::chrono_literals;

std::int32_t application::run(int argc, char** argv) {
    if (!parse_args(argc, argv)) {
        return 0;
    }

    std::signal(SIGTERM, application::terminate_handler);

    _sub_socket = std::make_unique<gcs::socket::socket>(_address, _sub_port, zmq::socket_type::sub);
    _sub_socket->subscribe("rotor_ctl");
    _sub_socket->set_receive_timeout(std::chrono::seconds(60));
    spdlog::info("Connected subscriber to {}:{}", _address, _sub_port);

    _pub_socket = std::make_unique<gcs::socket::socket>(_address, _pub_port, zmq::socket_type::pub);
    spdlog::info("Connected publisher to {}:{}", _address, _pub_port);

    _serial = std::make_unique<serial::serial>(_serial_port);
    _serial->set_baudrate(19200);
    spdlog::info("Connected serial to {}", _serial_port);

    message_handler::message_handler handler(std::move(_pub_socket), std::move(_serial));
    spdlog::info("gcs-rotator started");

    while (true) {
        if (auto lck = std::unique_lock(_mtx)) {
            if (!_is_running) {
                break;
            }
        }
        auto received = _sub_socket->receive();
        if (received.has_value()) {
            spdlog::info("Received topic '{}' with data '{}'", received->topic, received->data);
            handler.handle(*received);
            std::this_thread::sleep_for(10ms);
        }
    }

    auto pub_socket = handler.get_pub_socket();
    if (pub_socket) {
        pub_socket->close();
    }
    if (_sub_socket) {
        _sub_socket->close();
    }

    return 0;
}

bool application::parse_args(int argc, char** argv) {
    cxxopts::Options opts("gcs-rotator", "Rotator controller");

    // clang-format off
    opts.add_options()
      ("h,help", "Print usage")
      ("proxy", "Proxy adress", cxxopts::value<std::string>())
      ("pub", "Publisher port", cxxopts::value<std::string>())
      ("sub", "Subscriber port", cxxopts::value<std::string>())
      ("serial", "Serial port", cxxopts::value<std::string>());
    // clang-format on

    auto result = opts.parse(argc, argv);

    if (result.count("help")) {
        std::printf("%s\n", opts.help().c_str());
        return false;
    }

    try {
        _address = result["proxy"].as<std::string>();
        _pub_port = result["pub"].as<std::string>();
        _sub_port = result["sub"].as<std::string>();
        _serial_port = result["serial"].as<std::string>();
    } catch (const std::exception&) {
        std::printf("%s\n", opts.help().c_str());
        return false;
    }

    return true;
}

void application::stop() {
    std::lock_guard lck(_mtx);
    _is_running = false;
    gcs::socket::context().shutdown();
}

void application::terminate_handler(int sig) {
    static_cast<void>(sig);
    get_application().stop();
}

application& application::get_application() {
    static application app;
    return app;
}
} // namespace application
