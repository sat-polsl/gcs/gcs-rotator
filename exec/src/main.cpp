#include "application.hpp"

int main(int argc, char** argv) {
    auto& app = application::application::get_application();
    return app.run(argc, argv);
}