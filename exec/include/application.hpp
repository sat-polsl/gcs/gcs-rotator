#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <mutex>
#include <boost/asio/serial_port.hpp>

#include "gcs/socket/socket.hpp"
#include "message_handler/message_handler.hpp"
#include "serial/serial.hpp"

namespace application {
class application {
public:
    application(const application&) = delete;
    application& operator=(const application&) = delete;
    application(application&&) noexcept = delete;
    application& operator=(application&&) noexcept = delete;
    ~application() = default;
    std::int32_t run(int argc, char** argv);

    void stop();

    static void terminate_handler(int sig);

    static application& get_application();

private:
    application() = default;

    bool parse_args(int argc, char** argv);

    bool _is_running{true};
    std::mutex _mtx;

    std::string _address;
    std::string _pub_port;
    std::string _sub_port;
    std::string _serial_port;

    std::unique_ptr<gcs::socket::socket> _pub_socket;
    std::unique_ptr<gcs::socket::socket> _sub_socket;
    std::unique_ptr<serial::serial> _serial;
};
} // namespace application
